﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RECREGM1703609.Classes
{
    public class Cliente : Pessoa
    {
        public Cliente(double limiteCredito, int cartaoCredito, string contato, bool status)
        {
            this.limiteCredito = limiteCredito;
            this.cartaoCredito = cartaoCredito;
            this.contato = contato;
            this.status = status;
        }

        private double limiteCredito { get; set; } 
        private int cartaoCredito { get; set; }
        private string contato { get; set; }
        private Boolean status { get; set; }

        public double verificaCredito ()
        {
        }

        public int validaCartao ()
        {

        }
    }
}
